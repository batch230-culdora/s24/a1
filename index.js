// console.log("Hello There, and I'm so sorry!");


/*Activity:
1. In the S24 folder, create an activity folder and an index.html and index.js file inside of it.
2. Link the script.js file to the index.html file.

*/

// 3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…

let anyNum = 2;
const getCube = anyNum ** 3;
console.log(`The cube of ${anyNum} is ${getCube}`);


// =====================================================

// 5. Create a variable address with a value of an array containing details of an address.
// 6. Destructure the array and print out a message with the full address using Template Literals.

const completeAddress = [258, "Washington Ave NW,", "California", 90011];
const [houseNumber, avenue, state, zipcode] = completeAddress;
console.log(`I live at ${houseNumber} ${avenue} ${state} ${zipcode}`);


// =====================================================

// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
// 8. Destructure the object and print out a message with the details of the animal using Template Literals.

const animal = {
    name: "Lolong",
    kind: "saltwater crocodile",
    weight: 1075,
    feet: 20,
    inches: 3,
}
const {name, kind, weight, feet, inches} = animal;
console.log(`${name} was a ${kind}. He weighed at ${weight} kgs with a measurement of ${feet} ft ${inches} in.`);


// =====================================================

// 9. Create an array of numbers.
// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

const numbers = [1, 2, 3, 4, 5];
numbers.forEach((numbers) => {
    console.log(`${numbers}`);
});


// =====================================================

// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.

let iteration = 0;
let reducedNumber = numbers.reduce((acc,cur) => {
    return acc + cur;
    ++iteration;
});
console.log(`${reducedNumber}`);

// =====================================================

// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
// 13. Create/instantiate a new object from the class Dog and console log the object.

class Dog{
    constructor(name, age, breed){
    this.name = name;
    this.age = age;
    this.breed = breed;
    }
}

const myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);

/*
14. Create a git repository named S24.
15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
16. Add the link in Boodle.*/
